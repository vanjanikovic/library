import React, { useContext } from "react";
import { UserContext } from "../Context";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Hero from "../components/Hero";
import Recommended from "../components/Recommended";

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoadingVideos: true,
      videos: [],
    };
  }
  static contextType = UserContext;

  render() {
    return (
      <div>
        <Hero title="Library">
          <p style={{ textAlign: "center" }}>
            Ovo je biblioteka i ovp sve menjamo u home-u
          </p>
        </Hero>
        <Recommended />
      </div>
    );
  }
}
