import React, { Component } from "react";
import Hero from "../components/Hero";
import Title from "../components/Title";
import Comment from "../components/Comment";
import FormComment from "../components/CommentForm";
class BookPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      isLoading: true,
      video: {},
      comments: [],
    };
  }
  componentDidMount() {
    fetch("https://localhost:44367/api/book/" + this.state.id)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          isLoading: false,
          video: data,
        });
      });
    fetch("https://localhost:44367/api/comm/" + this.state.id)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          comments: data,
        });
      });
  }
  render() {
    return (
      <div
        style={{
          backgroundColor: "#FAACA8",
          backgroundImage: "linear-gradient(80deg, #FAACA8 0%, #DDD6F3 80%)",
        }}
      >
        <Hero title={this.state.video.Name}></Hero>
        <div className="container ">
          <div className="row">
            <div className="col-lg-5">
              <img
                src="https://images-na.ssl-images-amazon.com/images/I/61d1QJ0tPhL.jpg"
                style={{ maxWidth: "350px" }}
              />
            </div>
            <div className="col-lg-7 text-center">
              <h4
                className="text-white"
                style={{ letterSpacing: "1.5px", textTransform: "uppercase" }}
              >
                Author
              </h4>
              <p>{this.state.video.Author}</p>
              <h4
                className="text-white"
                style={{ letterSpacing: "1.5px", textTransform: "uppercase" }}
              >
                Genre
              </h4>
              <p>Zanr</p>
              <h4
                className="text-white"
                style={{ letterSpacing: "1.5px", textTransform: "uppercase" }}
              >
                Description
              </h4>
              <p className="text-center">{this.state.video.Description}</p>
              {this.state.video.Available ? (
                <h5 className="text-success">Available...</h5>
              ) : (
                <h5 className="text-muted">Not available...</h5>
              )}
            </div>
          </div>
        </div>
        <div className="container m-4">
          <h3
            className="text-center m-4 text-white"
            style={{ letterSpacing: "1.5px", textTransform: "uppercase" }}
          >
            Comments
          </h3>

          {this.state.comments.map((x) => (
            <Comment key={x.Id} author={x.Nick} text={x.Text} />
          ))}
        </div>
        <div className="container m-4">
          <FormComment bookId={this.state.id} />
        </div>
      </div>
    );
  }
}

export default BookPage;
