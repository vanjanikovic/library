import React, { Component } from "react";
import Title from "../components/Title";
import Book from "../components/Book";
import Row from "react-bootstrap/esm/Row";
import Search from "../components/Search";
class Books extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      author: "",
      bookName: "",
      isLoading: true,
      books: [],
    };
  }

  componentDidMount() {
    fetch("https://localhost:44367/api/books")
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          isLoading: false,
          books: data,
        });
      });
  }
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }
  handleSearch(event) {
    event.preventDefault();
    const data = { Author: this.state.author, Name: this.state.bookName };

    fetch("https://localhost:44367/api/search", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        this.setState({ books: data });
      })
      .catch((error) => {
        console.log("Error:", error);
      });
  }
  render() {
    return (
      <div
        style={{
          backgroundColor: "#FAACA8",
          backgroundImage: "linear-gradient(80deg, #FAACA8 0%, #DDD6F3 80%)",
        }}
      >
        <Title title="Books" />
        <div className="container">
          <div className="row justify-content-center text-white">
            <div className="form-group m-4">
              <h4>Search</h4>
            </div>
            <form className="form-inline">
              <div className="form-group m-2">
                <label>Name:</label>
                <input
                  type="text"
                  name="bookName"
                  value={this.state.bookName}
                  className="form-control"
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group m-2">
                <label>Author:</label>
                <input
                  type="text"
                  value={this.state.author}
                  className="form-control"
                  name="author"
                  onChange={this.handleChange}
                />
              </div>
              <button
                className="btn btn-outline-dark"
                onClick={this.handleSearch}
              >
                Search...
              </button>
            </form>
          </div>
        </div>
        <div className="container">
          <Row>
            {this.state.books.map((x) => (
              <Book key={x.Id} title={x.Name} id={x.Id} />
            ))}
          </Row>
        </div>
      </div>
    );
  }
}

export default Books;
