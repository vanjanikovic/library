import React, { Component, useState } from "react";
import Home from "./pages/Home";
import Error from "./pages/Error";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch, Link } from "react-router-dom";
import { UserContext } from "./Context";
import "./App.css";
import NavBar from "./components/NavBar";
import BookPage from "./pages/BookPage";
import Books from "./pages/Books";

function App() {
  const [value, setValue] = useState("");
  return (
    <>
      <UserContext.Provider value={{ value, setValue }}>
        <NavBar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/books" exact component={Books} />
          <Route path="/book/:id" exact component={BookPage} />
          <Route component={Error} />
        </Switch>
      </UserContext.Provider>
    </>
  );
}

export default App;
