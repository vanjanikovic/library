import React from "react";
export default function Comment({ author, text }) {
  return (
    <div className="row">
      <div className="col-lg-3"></div>
      <div className="col-lg-6">
        <h4>{author ? author : "Anonymous"}</h4>
        <p>{text}</p>
      </div>
    </div>
  );
}
