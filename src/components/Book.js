import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/esm/Col";
import { Link } from "react-router-dom";

export default function Book({ id, title }) {
  return (
    <div
      className="m-4"
      style={{
        fontFamily: "sans",
        textAlign: "center",
        boxShadow: "5px 1px 10px 4px #888888",
      }}
    >
      <Card style={{ width: "14rem" }}>
        <Card.Img
          variant="top"
          bg="dark"
          src="https://images-na.ssl-images-amazon.com/images/I/61d1QJ0tPhL.jpg"
          style={{ maxHeight: "14rem" }}
        />
        <Card.Body>
          <Card.Title style={{ letterSpacing: "2.5px" }}>{title}</Card.Title>
          <Link
            className="btn btn-dark"
            to={`/book/${id}`}
            style={{
              textTransform: "uppercase",
              fontFamily: "Segoe UI",
              letterSpacing: "3px",
              fontSize: "12px",
            }}
          >
            Details...
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
}
