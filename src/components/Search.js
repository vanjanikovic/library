import React, { Component } from "react";
class Search extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      author: "",
      bookName: "",
    };
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }
  handleSearch() {
    const data = { Author: this.state.author, Name: this.state.bookName };

    fetch("https://localhost:44367/api/search", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", data);
      })
      .catch((error) => {
        console.log("Error:", error);
      });
  }
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center text-white">
          <div className="form-group m-4">
            <h4>Search</h4>
          </div>
          <form className="form-inline">
            <div className="form-group m-2">
              <label>Name:</label>
              <input
                type="text"
                name="bookName"
                value={this.state.bookName}
                className="form-control"
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group m-2">
              <label>Author:</label>
              <input
                type="text"
                value={this.state.author}
                className="form-control"
                name="author"
                onChange={this.handleChange}
              />
            </div>
            <button
              className="btn btn-outline-dark"
              onClick={this.handleSearch}
            >
              Search...
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Search;
