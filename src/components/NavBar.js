import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <Navbar bg="dark" variant="dark" className="">
      <Link
        to="/"
        className="navbar-brand"
        style={{
          marginLeft: "40%",
          textTransform: "uppercase",
          letterSpacing: "4px",
        }}
      >
        Library
      </Link>
      <Nav className="mr-auto">
        <Link
          to="/books"
          className="nav-link"
          style={{ textTransform: "uppercase", letterSpacing: "4px" }}
        >
          Books
        </Link>
      </Nav>
    </Navbar>
  );
}
