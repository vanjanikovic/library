import React from "react";
export default function Title({ title }) {
  return (
    <h3
      style={{
        textAlign: "center",
        fontSize: "40px",
        textTransform: "uppercase",
        letterSpacing: "4px",
        textDecorationThickness: "3px",
        fontFamily: "times",
        textDecoration: "underline",
      }}
    >
      {title}
    </h3>
  );
}
