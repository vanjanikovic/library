import React, { Component } from "react";
class FormComment extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleButton = this.handleButton.bind(this);
    this.state = {
      nickname: "",
      text: "",
    };
  }
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }
  handleButton(event) {
    event.preventDefault();
    const data = {
      Nick: this.state.nickname,
      Text: this.state.text,
      BookId: this.props.bookId,
    };
    console.log(data);
    fetch("https://localhost:44367/api/comments", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.log("Error:", error);
      });
    this.setState({ nickname: "", text: "" });
  }
  render() {
    return (
      <div className="row justify-content-center">
        <div className="col-lg-6 text-white">
          <h4
            className="text-center"
            style={{ letterSpacing: "1.5px", textTransform: "uppercase" }}
          >
            Add new comment
          </h4>
          <form className="form">
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                value={this.state.nickname}
                onChange={this.handleChange}
                name="nickname"
                type="text"
              />
            </div>
            <div className="form-group">
              <label>Comment</label>
              <textarea
                className="form-control"
                value={this.state.text}
                onChange={this.handleChange}
                name="text"
                type="text"
              />
            </div>
            <button
              type="button"
              className="btn btn-outline-dark"
              onClick={this.handleButton}
            >
              Add
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default FormComment;
