import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Title from "./Title";
export default function Hero({ children, title }) {
  return (
    <Jumbotron>
      <Title title={title} />
      {children}
    </Jumbotron>
  );
}
