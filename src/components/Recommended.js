import React, { Component } from "react";
import Title from "./Title";
import Book from "./Book";
import Row from "react-bootstrap/esm/Row";
class Recommended extends Component {
  state = {
    isLoading: true,
    videos: [],
  };
  componentDidMount() {
    fetch("https://localhost:44367/api/recommended")
      .then((res) => res.json())
      .then((data) => {
        console.log(data[0]);
        this.setState({
          isLoading: false,
          videos: data,
        });
      });
    console.log(this.state.videos);
  }
  render() {
    return (
      <div>
        <Title title="Recommended" />
        <Row className="m-4 justify-content-center">
          {this.state.videos.map((book) => (
            <Book key={book.Id} title={book.Name} id={book.Id} />
          ))}
        </Row>
      </div>
    );
  }
}

export default Recommended;
